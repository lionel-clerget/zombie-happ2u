class Position {
	constructor(elem, playground) {
		this.playground = playground;
		this.elem = elem;
		this.yMin = elem.offsetTop //+ 35;
		this.xMin = elem.offsetLeft //+ 50;
		this.width = elem.width;
		this.height = elem.height;
		this.xMax = this.xMin + this.width// - 160;
		this.yMax = this.yMin + this.height //- 55;
		this.collision = null;
	}
	is_collision(touchY, touchX) {
		if (touchY >= this.yMin && touchY <= this.yMax) {
			if (touchX >= this.xMin && touchX <= this.xMax) {
				this.collision = true;
			} else {
				this.collision = false;
			}
		} else {
			this.collision = false;
		}

		return this.collision
	}
}

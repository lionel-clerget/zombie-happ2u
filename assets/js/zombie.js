class Zombie {
    constructor(id) {
        this.folder=["01-Idle","02-Walk","04-Hurt","05-Die"];
        this.pre_file=[
            "__Zombie01_Idle_",
            "__Zombie01_Walk_",
            "__Zombie01_Hurt_",
            "__Zombie01_Die_"
        ];
        this.state = {
            "idle" : 0,
            "walk" : 1,
            "hurt" : 2,
            "die" : 3
        }
        // Data
        this.elem = document.getElementById(id);
        this.path = `./assets/img/zombie/${this.folder[this.current_state]}/${this.pre_file[this.current_state]}${this.frame}.png`;
        this.interval = null;
        // Animation
        this.maxFrame = 7;
        this.frame = 0;

        // HpBar
        this.hpBar = document.getElementById('hpBar');

        // Bood
        this.blood = document.getElementById('blood');

        // Zombie State
        this.current_state = 0;
        this.is_alive = true;
        this.is_hurt = false;
        this.hp = 100;

        this._init();
    }


    _init(){
        this.elem.src = this.path;
    }

    get_anim_maxFrame(){
        if (this.folder[this.current_state] == "01-Idle" || this.folder[this.current_state] == "03-Attack") {
            return 11;
        }else {
            return 7;
        }
    }

    set_current_state(_new_state){
        this.current_state = _new_state;
        this.frame = 0;
    }

    play(){
        if (this.get_anim_maxFrame() != this.maxFrame) {
            this.maxFrame = this.get_anim_maxFrame();
        }
        if (this.frame < this.maxFrame) {
            this.frame++;
        }else {
            switch (this.current_state) {
                case this.state.die:
                    this.is_alive = false;
                    break;

                case this.state.hurt:
                    this.current_state = this.state.idle;
                    this.set_blood(false);
                    this.frame = 0;
                    break;

                default:
                    this.frame = 0;
            }
        }
        this.path = `./assets/img/zombie/${this.folder[this.current_state]}/${this.pre_file[this.current_state]}${this.frame}.png`;
        this.elem.src = this.path;
    }

    remove_hp(_toRemove){
        if (this.hp > 0 && this.hp > _toRemove) {
            this.hp -= _toRemove;
            this.set_current_state(this.state.hurt);
            this.set_blood(true);
            this.set_hpBar_width(this.hp);
        }else {
            this.hp = 0;
            this.set_hpBar_width(this.hp);
            this.set_current_state(this.state.die);
        }
    }

    change_hpText(_value){
        document.getElementById('hpText').innerHTML = `HP = ${_value}`;
    }

    run_animation(){
        this.interval = setInterval(()=>{
            if (zombie.is_alive) {
                zombie.play();
            }else {
                setTimeout(this.reanimate, 2000);
            }
        }, 100)
    }

    revive(){
        if (this.frame > 0) {
            this.frame--;
        }else {
            this.current_state = 0;
            this.is_alive = true;
            this.reset_zombie();
        }
        this.path = `./assets/img/zombie/${this.folder[this.current_state]}/${this.pre_file[this.current_state]}${this.frame}.png`;
        this.elem.src = this.path;
    }

    set_hpBar_width(_value){
        this.hpBar.style.width = `${_value}%`;
        this.animate_hpBar();
        this.change_hpText(_value)
    }

    animate_hpBar(){
        var bar = document.getElementById('hpBar-container');
        bar.classList.add("tremble");
        setTimeout(this.reset_anim_hpBar, 250);
    }
    reset_anim_hpBar(){
        var bar = document.getElementById('hpBar-container');
        bar.classList.remove("tremble");
    }

    set_blood(_is_display){

        if (_is_display) {
            this.blood.style.display = "block";
        }else {
            this.blood.style.display = "none";
        }
    }

    reset_zombie(){
        this.is_alive = true;
        this.hp = 100;
        this.set_hpBar_width(this.hp);
        this.set_blood(false);
        this.frame = 0;
    }
}

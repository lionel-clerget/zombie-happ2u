// ZOMBIE
var zombie = new Zombie("zombie");
// Run each animation frame
var zombie_animaton = setInterval(()=>{
    if (zombie.is_alive) {
        // play Idle animation
        zombie.play();
    }else {
        // play Die animation
        setTimeout(zombie.revive(), 2000);
    }
},100);

HapticJS.Socket.onready(() => {
  HapticJS.Message.Stop();

  var weapons = [
    new Weapon("./assets/img/weapons/buzz_saw.svg", "Divine Slicer", "blade"),
    new Weapon("../img/weapons/hammer.png", "Undead Crusher", "hammer")
  ];

  var activeTexture = 0;

  weapons[0].setWaveform(100, 950, 100, 100, 400);
  weapons[1].setState(true);
  weapons[1].setWaveform(100, 1250, 100, 426, 427);

  weapons[0].elem.addEventListener("touchstart", () => {
    weapons[0].setState(true);
    weapons[1].setState(false);
  });

  weapons[1].elem.addEventListener("touchstart", () => {
    weapons[0].setState(false);
    weapons[1].setState(true);
  });

  var bgwaveform = new HapticJS.Waveform(100, 9000, 1, 380, 610);
  var bgfeedback = new HapticJS.Feedback(
    HapticJS.SQUARE,
    bgwaveform,
    HapticJS.TEMPORAL,
    0
  );
  var bgTexture = new HapticJS.Texture(false, 0);
  bgTexture.setFrontFeedback(bgfeedback);

  var zombiePos = new Position(
    document.getElementById("zombie"),
    document.getElementById("bg")
  );
  var textureZombie = new HapticJS.Texture(false, 0);
  zombiePos.playground.addEventListener("touchmove", e => {
    for (const active of weapons) {
      // console.log(active.state)
      if (active.state === true)
        var feedback = new HapticJS.Feedback(
          HapticJS.SQUARE,
          active.waveform,
          HapticJS.SPATIAL,
          0
        );
      textureZombie.setFrontFeedback(feedback);
    }
    var plan = false;

    touchX = e.touches[0].clientX;
    touchY = e.touches[0].clientY;

    if (zombiePos.is_collision(touchY, touchX)) {
      if (activeTexture !== 0) {
        HapticJS.Message.Stop();
        HapticJS.Message.SetTexture(textureZombie);

        if (!zombie.is_hurt) {
            zombie.remove_hp(25);
            zombie.is_hurt = true;
        }
        activeTexture = 0;
      }
    } else {
        zombie.is_hurt = false;
      if (activeTexture !== 10) {
        HapticJS.Message.Stop();
        HapticJS.Message.SetTexture(bgTexture);
        activeTexture = 10;
        plan = true;
      }
      //HapticJS.Message.Stop();
    }
  });
});

class Weapon {
    /**
     *
     * @param {string} src
     * @param {string} name
     * @param {string} elem
     */
    constructor(src, name, elem){
        this.elem = document.getElementById(elem);
        this.name = name
        this.src = src;
        this.state = false;
        this.waveform = null;
        this._init();
    }

    _init(){
        this.elem.src = this.src;
    }
    /**
     *
     * @param {number} maxAmp
     * @param {number} period
     * @param {number} attack
     * @param {number} sustain
     * @param {number} release
     */
    setWaveform(maxAmp, period, attack, sustain, release) {
        this.waveform = new HapticJS.Waveform(maxAmp, period, attack, sustain, release)
    }
    /**
     *
     * @param {boolean} bool
     */
    setState(bool) {
        this.state = bool;
    }
}
